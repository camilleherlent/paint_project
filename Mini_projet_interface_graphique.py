from tkinter import *
import serial
import numpy as np
#Interface pour avoir un choix de couleur
from tkinter.colorchooser import askcolor

ser = serial.Serial("COM5",baudrate=115200, timeout=0.01, writeTimeout=0.1)

print(ser)


    
class Paint(object):

    DEFAULT_PEN_SIZE = 5.0
    DEFAULT_COLOR = 'black'
    
    

    def __init__(self):
        
        #Définition de la fenêtre
        self.root = Tk()
        global fenetre 
        fenetre= self.root

        #Choix outils
        self.pen_button = Button(self.root, text='pen', command=self.use_pen)
        self.pen_button.grid(row=0, column=0)


        self.color_button = Button(self.root, text='color', command=self.choose_color)
        self.color_button.grid(row=0, column=1)

        self.eraser_button = Button(self.root, text='eraser', command=self.use_eraser)
        self.eraser_button.grid(row=0, column=2)
        
        self.register_button = Button(self.root, text='register', command=self.register_image)
        self.register_button.grid(row=0, column=3)

        self.choose_size_button = Scale(self.root, from_=1, to=10, orient=HORIZONTAL)
        self.choose_size_button.grid(row=0, column=4)

        #Définition fenêtre
        self.c = Canvas(self.root, bg='white', width=480, height=272)
        self.c.grid(row=1, columnspan=5)

        self.setup()
        #self.root.after(10, self.root.serie()) 
        self.root.mainloop()
    
    
    
    #Définition paramètres initiaux
    def setup(self):
        self.old_x = None
        self.old_y = None
        self.line_width = self.choose_size_button.get()
        self.color = self.DEFAULT_COLOR
        self.eraser_on = False
        self.active_button = self.pen_button
        self.c.bind('<B1-Motion>', self.paint)
        self.c.bind('<ButtonRelease-1>', self.reset)

    def use_pen(self):
        self.activate_button(self.pen_button)

    def choose_color(self):
        self.eraser_on = False
        self.color = askcolor(color=self.color)[1]

    def use_eraser(self):
        self.activate_button(self.eraser_button, eraser_mode=True)
        
    def register_image(self):
        filename = 'image.png'
        self.save(filename)

    #Design bouton
    def activate_button(self, some_button, eraser_mode=False):
        self.active_button.config(relief=RAISED)
        some_button.config(relief=SUNKEN)
        self.active_button = some_button
        self.eraser_on = eraser_mode
        
    #def serie(self):
        

    def paint(self):
        #global x0_lu, y0,lu
        x = ser.readlines()          # read one byte
        print("ok")
        if x!=[]:
            decodage = x[0].decode('utf-8')
            y0_lu = int(decodage[6:9])
            x0_lu = int(decodage[1:4])
            print(x0_lu,y0_lu)
         
        self.line_width = self.choose_size_button.get()
        paint_color = 'white' if self.eraser_on else self.color
        if self.old_x and self.old_y:
            self.c.create_line(self.old_x, self.old_y, x0_lu, y0_lu,
                               width=self.line_width, fill=paint_color,
                               capstyle=ROUND, smooth=TRUE, splinesteps=36)
        self.old_x = x0_lu
        self.old_y = y0_lu
        
        self.root.after(10, self.root.paint())

    def reset(self, event):
        self.old_x, self.old_y = None, None


if __name__ == '__main__':
    Paint()
    
