
import numpy as np
from tkinter import *
import serial
from tkinter.colorchooser import askcolor

ser = serial.Serial("COM5",baudrate=115200, timeout=0.01, writeTimeout=0.1)

print(ser)



class Paint():
    
    DEFAULT_PEN_SIZE = 5.0
    DEFAULT_COLOR = 'black'
    
    def __init__(self,root):
        self.root = root
        self.root.geometry("480x272")
        self.root.configure(background = "white")
        self.root.resizable(0,0)
        
         #Choix outils
        self.pen_button = Button(self.root, text='pen', command=self.use_pen)
        self.pen_button.grid(row=0, column=0)


        self.color_button = Button(self.root, text='color', command=self.choose_color)
        self.color_button.grid(row=0, column=1)

        self.eraser_button = Button(self.root, text='eraser', command=self.use_eraser)
        self.eraser_button.grid(row=0, column=2)

        self.choose_size_button = Scale(self.root, from_=1, to=10, orient=HORIZONTAL)
        self.choose_size_button.grid(row=0, column=3)

        #Définition fenêtre
        self.c = Canvas(self.root, bg='white', width=480, height=272)
        self.c.grid(row=1, columnspan=4)
        
        #Paramètres initiaux
        self.x = 0
        self.y = 0
        self.zoom = 1
        self.line_width = 1
        self.color = self.DEFAULT_COLOR
        self.eraser_on = False
        self.active_button = self.pen_button
        self.root.after(1, self.paint)
        
    
    def use_pen(self):
        self.activate_button(self.pen_button)

    def choose_color(self):
        self.eraser_on = False
        self.color = askcolor(color=self.color)[1]

    def use_eraser(self):
        self.activate_button(self.eraser_button, eraser_mode=True)

    #Design bouton
    def activate_button(self, some_button, eraser_mode=False):
        self.active_button.config(relief=RAISED)
        some_button.config(relief=SUNKEN)
        self.active_button = some_button
        self.eraser_on = eraser_mode
        
    
    def paint(self):
        
        recu = ser.readlines()          # read one byte

        if recu!=[]:
            for i in range(0,len(recu)):
                decodage = recu[0].decode('utf-8')
                self.x = int(decodage[1:4])
                self.y = int(decodage[6:9])
                self.zoom = int(decodage[11:13])
                self.line_width = int(decodage[15:17])
        
                x1,y1 = self.x - self.line_width, self.y - self.line_width
                x2,y2 = self.x + self.line_width, self.y + self.line_width
                paint_color = 'white' if self.eraser_on else self.color
                self.c.create_oval(x1, y1, x2, y2, fill=paint_color,
                               outline = paint_color)
        
        self.root.after(1, self.paint)
 


if __name__ == "__main__":
    root = Tk()
    p = Paint(root)
    root.mainloop()
    

ser.close()

# Port série COM5
# Vitesse de baud : 115200
# Timeout en lecture : 0.01 sec
# Timeout en ériture : 0.1 sec


#ser = serial.Serial("COM5",baudrate=115200, timeout=0.01, writeTimeout=0.1)
#
#print(ser)
#
#def serie():
#    global x0_lu, y0,lu
#    x = ser.readlines()          # read one byte
#    if x!=[]:
#        decodage = x[0].decode('utf-8')
#        y0_lu = int(decodage[6:9])
#        x0_lu = int(decodage[1:4])
#        Tableau[x0_lu,y0_lu]=1
#        print(x0_lu,y0_lu)
#    fenetre.after(10, serie)  
#    
#fenetre.after(10, serie)    
#        
#fenetre.mainloop()
#
#ser.close()